#ifndef VECTORCL_H
#define VECTORCL_H

#include <ctime>
#include <iostream>

using std::cout;
using std::cin;
using std::endl;

template <class Type>
class VectorClass
{
    private:
        Type *massiv;
        int num; //размер массива
        int NewNum;//"Промежуточная" переменная, необходимая для изменения размера массива

    public:
        VectorClass ();
        void VyvodMas();
        void Sort();//Сортировка Шелла
        void push_back(const Type &NewElement);
        void pop_back();
        void push_front(const Type &DeletedElement);
        void pop_front();
        size_t size() const;
        Type at(int Position);
        Type front();
        Type back();
        void empty() const;
        void clear();
        void ModifyVector();

        Type& operator[](int AnySimbol);
};

template <typename Type>
VectorClass<Type>::VectorClass()
{
    NewNum = num = 0;
    massiv = new Type[num];


}


template <typename Type>
void VectorClass<Type>::VyvodMas()
{
    cout<<endl;
    for (int VM=0;VM<NewNum;VM++)
    {
        cout<<massiv[VM]<<" ";
    }
    cout<<endl;
}



template <typename Type>
void VectorClass<Type>::Sort() //Сортировка Шелла
{
    cout<<endl<<"Sortirovka Shella: ";
    VyvodMas();
    int shag = num/2;
    while (shag>0)
    {
        for (int i=0;i<num-shag;i++)
        {
            int j=i;
            while (j >= 0 && massiv[j] > massiv[j + shag])
            {
                Type vrem = massiv[j];
                massiv[j]=massiv[j+shag];
                massiv[j+shag]=vrem;
                j--;
            }
        }
        VyvodMas();
        shag=shag/2;
    }
    cout<<endl;
}

template <typename Type>
void VectorClass<Type>::ModifyVector()
{       
        Type *massiv2 = new Type[NewNum];

        if(NewNum>num)
        {
            for (int i=0; i<NewNum-1;i++)
            {
                massiv2[i]=massiv[i];
            }

        }
        else
        {
            for (int i=0; i<NewNum;i++)
            {
                massiv2[i]=massiv[i];
            }
        }
        delete [] massiv;
        massiv = massiv2;
}




template <typename Type>
void VectorClass<Type>::push_back(const Type &NewElement) //добавление элемента в конец
{
    //cout<<endl<<"Dobavlenie elementa v konec: ";
    NewNum=num+1;
    ModifyVector();
    massiv[num] = NewElement;
    num=NewNum;
    //VyvodMas();
}

template <typename Type>
void VectorClass<Type>::pop_back() //Удаление последнего элемента
{
    //cout<<endl<<"Udalenie poslednego elementa: "<<endl;
    NewNum = num-1;
    ModifyVector();
    num=NewNum;
    //VyvodMas();
}

template <typename Type>
void VectorClass<Type>::push_front(const Type &DeletedElement) //Добавление элемента в начало
{
    NewNum=num+1;
    Type *PushFront = new Type[NewNum];
    for (int i=0;i<num;i++)
    {
        PushFront[i+1]=massiv[i];
    }
    //cout<<endl<<"Dobavlenie elementa v nachalo: ";
    PushFront[0]=DeletedElement;
    delete [] massiv;
    massiv = PushFront;
    //VyvodMas();
    num=NewNum;
}

template <typename Type>
void VectorClass<Type>::pop_front() //Удаление первого элемента
{
    NewNum=num-1;
    Type *PopFront = new Type [NewNum];
    for (int i=1;i<num;i++)
    {
        PopFront[i-1]=massiv[i];
    }
    //cout<<endl<<"Udalenie pervogo elementa: ";
    delete [] massiv;
    massiv = PopFront;
    //VyvodMas();
    num=NewNum;
}

template <typename Type>
size_t VectorClass<Type>::size() const //Вывод размера
{
    return num;
}

template <typename Type>
Type VectorClass<Type>::at(int Position) //Вывод конкретного элемента
{
    if (Position<num+1)
    {
        Type j = massiv[Position-1];
        return j;
    }
    else throw "Error";
}

template <typename Type>
Type VectorClass<Type>::front() //Вывод конкретного элемента
{   
    //cout<<"Perviy element massiva = "<<massiv[0]<<endl;
    return massiv[0];
}

template <typename Type>
Type VectorClass<Type>::back() //Вывод конкретного элемента
{
    //cout<<"Posledniy element massiva = "<<massiv[num-1]<<endl;
    return massiv[num-1];
}

template <typename Type>
void VectorClass<Type>::clear() //Очистка массива
{
    cout<<"Zachistka: ";
    num=NewNum=0;
    ModifyVector();
    for (int VM=0;VM<num;VM++)
    {
        cout<<massiv[VM]<<" ";
    }
    cout<<endl;
}


template <typename Type>
void VectorClass<Type>::empty() const //Проверка на пустоту
{
    bool emptyMas;
    emptyMas=num;
    if (emptyMas==!0) emptyMas = true;
    else emptyMas = false;

    if (emptyMas == true) cout<<"Massiv ne pustoy"<<endl;
    else cout<<"Massiv pustoy"<<endl;
}



template <typename Type>
Type& VectorClass<Type>::operator[](int AnySimbol)
{
    if(AnySimbol>=0&&AnySimbol<num)
    {
        return massiv[AnySimbol];
    }
    else
        throw "Vne granits vectora";
}

#endif // VECTORCL_H
